package config

import (
	"encoding/json"
	"fmt"
	"os"
)

type Config struct {
	Database struct {
		Hosts     		[]string `json:"hosts"`
		MongoHost		string `json:"mongohost"`
		MongoPort		string `json:"mongoport"`
		AuthDatabase 	string `json:"authdb"`
		AuthUserName 	string `json:"username"`
		AuthPassword 	string `json:"password"`
		DBName 			string `json:"dbname"`
	} `json:"database"`
	Host string `json:"host"`
	Port string `json:"port"`
}

func LoadConfiguration(file string) Config {
	var config Config
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		fmt.Println(err.Error())
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)
	return config
}