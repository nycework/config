package config

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"log"
)

var DB *mgo.Database
var mgoSession *mgo.Session

func MustConnectMongo() *mgo.Session{
	return ConnectMongo()
}

func InitDB() {
	defer func() {
		if e := recover(); e != nil {
			log.Println(e)
		}
	}()
}

var (
	username string
	password string
	host     string
	port     string
	instance string
)

func ConfigDB(config Config) {

	username = config.Database.AuthUserName
	password = config.Database.AuthPassword

	host = config.Database.MongoHost
	port = config.Database.MongoPort

	instance = config.Database.AuthDatabase
}

func ConnectMongo() *mgo.Session {
	conn := ""
	if len(username) > 0 {
		conn += username

		if len(password) > 0 {
			conn += ":" + password
		}

		conn += "@"
	}

	conn += fmt.Sprintf("%s:%s/%s", host, port, instance)
	if mgoSession == nil {
		var err error
		mgoSession, err = mgo.Dial(conn)
		if err != nil {
			log.Fatal("Failed to start the Mongo session")
		}
	}
	return mgoSession.Clone()
}